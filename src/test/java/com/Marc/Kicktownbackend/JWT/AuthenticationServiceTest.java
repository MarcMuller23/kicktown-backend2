package com.Marc.Kicktownbackend.JWT;

import com.Marc.Kicktownbackend.Repos.UserRepo;
import org.apache.catalina.security.SecurityConfig;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AuthenticationServiceTest {

    String jsonFormat = "{'username':'MarcMuller','password':'12345'}".replace("'", "\"");
    String falseJsonFormat = "{'username':'false','password':'user'}".replace("'", "\"");

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepo repository;


    @Test
    @org.junit.Test
    @Sql("classpath:test-data.sql")
    public void shouldNotAllowAccessToUnauthenticatedUsers() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/users")).andExpect(status().isForbidden());
    }


    //INTEGRATION TEST:
    @org.junit.Test
    @Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-If-We-Recieve-JWT-With-Existing-User_By-Checking-Status")
    public void shouldReturnAJWTToken() throws Exception{

        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk());
    }

    //INTEGRATION TEST:
    @org.junit.Test
    @Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-If-JWT-Is-Not-Null-With-Existing-User")
    public void jwtShouldNotBeNull() throws Exception{
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk())
                .andReturn();

        //Set JSON body in String
        String jwt = result.getResponse().getContentAsString();
        //Get rid of unused characters
        StringBuilder sb = new StringBuilder(jwt);
        sb.delete(0, 10);
        sb.deleteCharAt(sb.length() - 1);
        sb.deleteCharAt(sb.length() - 1);
        //Save actual JWT as new String
        String resultJwt = sb.toString();
        Assert.assertNotNull(resultJwt);
    }

    //INTEGRATION TEST:
    @Test
    @org.junit.Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-If-Bad-User-Cant-Sign-In_Should-Return-403")
    public void userShouldNotBeAbleToLogIn() throws Exception{
        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(falseJsonFormat))
                .andExpect(status().isForbidden());
    }

    //INTEGRATION TEST:
    @Test
    @org.junit.Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-If-User-Can-Get-To-Login-Endpoint-Without-JWT")
    public void userShouldNotBeAbleToVisitLoginEndpoint() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/login")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-If-User-Can-Get-To-Login-Endpoint-With-JWT")
    public void userShouldBeAbleToVisitLoginEndpoint() throws Exception{
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk())
                .andReturn();

        //Save JSON content to string
        String jwt = result.getResponse().getContentAsString();
        StringBuilder sb = new StringBuilder(jwt);
        sb.delete(0, 10);
        sb.deleteCharAt(sb.length()-1);
        sb.deleteCharAt(sb.length()-1);
        //Save actual JWT as String
        String resultJwt = sb.toString();

        mvc.perform(MockMvcRequestBuilders.get("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk());

    }

    @Test
    @Sql("classpath:test-data.sql")
    @org.junit.Test
    @DisplayName("Test-if-forbidden-user-can-access-/users-endpoint")
    public void userShouldNotBeAbleToVisitUsersEndpoint() throws Exception{
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(falseJsonFormat))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    @Sql("classpath:test-data.sql")
    @org.junit.Test
    @DisplayName("Test-If-User-Can-Access-Endpoint-When-Logged-In --> /users/ endpoint")

    public void userShouldBeAbleToVisitDifferentEndpoint() throws Exception{

        //STEP 1: ACCESS /AUTHENTICATE ENDPOINT WITH EXISTING USER
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk())
                .andReturn();

        //STEP 2: SAVE JSON RESPONSE IN STRING AND EXTRACT JWT
        //Save JSON content to string
        String jwt = result.getResponse().getContentAsString();
        StringBuilder sb = new StringBuilder(jwt);
        sb.delete(0, 10);
        sb.deleteCharAt(sb.length()-1);
        sb.deleteCharAt(sb.length()-1);
        //Save actual JWT as String
        String resultJwt = sb.toString();

        //STEP 3: ACCESS /LOGIN ENDPOINT USING AUTHORIZATION ENDPOINT WITH JWT IN HEADER
        mvc.perform(MockMvcRequestBuilders.get("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk());

        //STEP 4: ACCESS /USERS ENDPOINT USING THE SAME HEADER
        mvc.perform(MockMvcRequestBuilders.get("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk()); //THIS SHOULD PASS
    }

    @Test
    @Sql("classpath:test-data.sql")
    @org.junit.Test

    public void shouldBeAbleToVisitAuthenticateEndpoint() throws Exception{
        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk());
    }
}
