package com.Marc.Kicktownbackend.Repo;
import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Repos.UserRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;


import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
@ActiveProfiles("test")
@DataJpaTest
public class UserRepoTest {
    @Autowired
    private UserRepo userRepo;

    @Test
    public void shouldCreateUser(){
        UserDao user = new UserDao( 0, "Test", "User");
        UserDao createdUser = userRepo.save(user);
        assertThat(createdUser).usingRecursiveComparison().ignoringFields("id").isEqualTo(user);
    }

    @Test
    @Sql("classpath:test-repo-data.sql")
    public void shouldSaveUsersThroughSqlFile(){
        Optional<UserDao> test = userRepo.findById(10);
        assertThat(test).isNotEmpty();
    }

}
