package com.Marc.Kicktownbackend.JWTspring.Models;

public class AuthenticationResponse {

    private final int id;
    private final String token;

    public AuthenticationResponse(int id ,String token) {
        this.id=id;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
