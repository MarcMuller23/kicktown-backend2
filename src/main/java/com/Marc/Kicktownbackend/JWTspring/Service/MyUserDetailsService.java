package com.Marc.Kicktownbackend.JWTspring.Service;
import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Models.UserDto;
import com.Marc.Kicktownbackend.Repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepo userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        UserDao user = userDao.findByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
        /*return new User("foo", "foo", new ArrayList<>());*/

    }
    public int loadUserId(String username) throws UsernameNotFoundException{
        UserDao user = userDao.findByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return user.getId();
        /*return new User("foo", "foo", new ArrayList<>());*/

    }

    public UserDao save(UserDto user){
        UserDao newUser = new UserDao();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(user.getPassword());
        return userDao.save(newUser);
    }

}
