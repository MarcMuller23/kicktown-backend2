package com.Marc.Kicktownbackend.Service;

import com.Marc.Kicktownbackend.Models.ReviewDao;
import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Repos.ReviewRepo;
import com.Marc.Kicktownbackend.Repos.UserRepo;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class ReviewService {

    @Autowired
    private ReviewRepo reviewRepo;

    public List<ReviewDao> listAllReviews(){
        return reviewRepo.findAll();
    }

    public void createReview(ReviewDao review){
        reviewRepo.save(review);
    }

    public ReviewDao getReview(Integer id){
        return reviewRepo.findById(id).get();
    }


    public void deleteReview(Integer id){reviewRepo.deleteById(id);
    }


}
