package com.Marc.Kicktownbackend.Service;

import com.Marc.Kicktownbackend.Interface.IUserService;
import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Repos.UserRepo;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class UserService implements IUserService {

    @Autowired
    private UserRepo userRepo;

    public List<UserDao> listAllUsers(){
        return userRepo.findAll();
    }
    public void createUser(UserDao user){
        userRepo.save(user);
    }

    public UserDao getUser(Integer id){
        return userRepo.findById(id).get();
    }

    public UserDao getUserByUsername(String username){
        return userRepo.findByUsername(username);
    }

    public void deleteUser(Integer id){
        userRepo.deleteById(id);
    }
}
