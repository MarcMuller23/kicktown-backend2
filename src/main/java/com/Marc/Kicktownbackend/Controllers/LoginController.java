package com.Marc.Kicktownbackend.Controllers;

import com.Marc.Kicktownbackend.JWTspring.Models.AuthenticationRequest;
import com.Marc.Kicktownbackend.JWTspring.Models.AuthenticationResponse;
import com.Marc.Kicktownbackend.JWTspring.Service.MyUserDetailsService;
import com.Marc.Kicktownbackend.JWTspring.Util.JwtUtil;
import com.Marc.Kicktownbackend.Models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import com.Marc.Kicktownbackend.Service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@CrossOrigin("http://localhost:3030") //CORS

public class LoginController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    UserService userService;

    @CrossOrigin("http://localhost:3030") //CORS
    @RequestMapping({"/login"})
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Successfully logged in.");
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        final int userId = userDetailsService.loadUserId(authenticationRequest.getUsername());

        return ResponseEntity.ok(new AuthenticationResponse(userId,jwt));
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody UserDao user) {
        try {
            userService.createUser(user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
//kommentsadasd