package com.Marc.Kicktownbackend.Controllers;

import com.Marc.Kicktownbackend.Models.ReviewDao;
import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Repos.ReviewRepo;
import com.Marc.Kicktownbackend.Service.ReviewService;
import com.Marc.Kicktownbackend.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin("http://localhost:3030") //CORS
@RequestMapping("/reviews")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @GetMapping("")
    public List<ReviewDao> list() {
        return reviewService.listAllReviews();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReviewDao> get(@PathVariable Integer id) {
        try {
            ReviewDao user = reviewService.getReview(id);
            return new ResponseEntity<ReviewDao>(user, HttpStatus.OK);
        }
        catch (NoSuchElementException e) {
            return new ResponseEntity<ReviewDao>(HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody ReviewDao review, @PathVariable Integer id) {
        try {
            ReviewDao existReview = reviewService.getReview(id);
            review.setId(id);
            reviewService.createReview(review);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        reviewService.deleteReview(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createReview(@RequestBody ReviewDao review) {
        try {
            reviewService.createReview(review);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }



}
