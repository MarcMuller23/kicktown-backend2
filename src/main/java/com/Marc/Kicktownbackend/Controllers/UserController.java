package com.Marc.Kicktownbackend.Controllers;

import com.Marc.Kicktownbackend.Models.UserDao;
import com.Marc.Kicktownbackend.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin("http://localhost:3030") //CORS
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("")
    public List<UserDao> list() {
        return userService.listAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDao> get(@PathVariable Integer id) {
        try {
            UserDao user = userService.getUser(id);
            return new ResponseEntity<UserDao>(user, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<UserDao>(HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody UserDao user, @PathVariable Integer id) {
        try {
            UserDao existUser = userService.getUser(id);
            user.setId(id);
            userService.createUser(user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        userService.deleteUser(id);
    }
}

