package com.Marc.Kicktownbackend.Repos;

import com.Marc.Kicktownbackend.Models.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserDao,Integer> {
    UserDao findByUsername(String username);
}
