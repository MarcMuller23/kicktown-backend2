package com.Marc.Kicktownbackend.Repos;

import com.Marc.Kicktownbackend.Models.ReviewDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepo extends JpaRepository<ReviewDao,Integer> {

}
