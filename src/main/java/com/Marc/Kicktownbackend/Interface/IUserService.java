package com.Marc.Kicktownbackend.Interface;

import com.Marc.Kicktownbackend.Models.UserDao;

import java.util.List;

public interface IUserService {

    List<UserDao> listAllUsers();
    void createUser(UserDao user);
    UserDao getUser(Integer id);
    void deleteUser(Integer id);


}
