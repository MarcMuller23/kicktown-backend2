package com.Marc.Kicktownbackend.Models;

import javax.persistence.*;

@Entity
@Table(name = "review")
public class ReviewDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String playername;
    @Column
    private String reviewtext;
    @Column
    private String reviewername;


    //Constructor used for Testing methods.
    public ReviewDao(int id, String playerName, String reviewText, String reviewerName) {
        this.id = id;
        this.playername = playerName;
        this.reviewtext = reviewText;
        this.reviewername = reviewerName;

    }

    //Constructor user for instantiating this class.
    public ReviewDao(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playername;
    }

    public void setPlayerName(String playerName) {
        this.playername = playerName;
    }

    public String getReviewText() {
        return reviewtext;
    }

    public void setReviewText(String reviewText) {
        this.reviewtext = reviewText;
    }

    public String getReviewerName() {
        return reviewername;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewername = reviewerName;
    }
}
