package com.Marc.Kicktownbackend.Models;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String username;
    @Column
    private String password;


    //Constructor used for Testing methods.
    public UserDao(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;

    }

    //Constructor user for instantiating this class.
    public UserDao(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
